#define MyAppName "Omsipath"
#define MyAppVersion "1.1"
#define MyAppPublisher "Lukas G."      
#define MyAppURL "https://gitlab.com/omsi2/omsipath"
#define MyAppExeName "Omsipath.exe"

[Setup] 
AllowNetworkDrive=no
AllowNoIcons=yes
AllowUNCPath=no 
AppCopyright=(c) {#MyAppPublisher} All rights reserved.    
AppId={{90B67BC3-9B88-4047-B38C-D2349E2D86F2}    
AppName={#MyAppName}
AppPublisher={#MyAppPublisher}     
AppUpdatesURL={#MyAppURL}    
AppVerName={#MyAppName} {#MyAppVersion}     
AppVersion={#MyAppVersion}            
Compression=none      
DefaultDialogFontName=Segoe UI
DefaultDirName={autopf}\{#MyAppName}     
DefaultGroupName={#MyAppName}        
DisableWelcomePage=no
OutputBaseFilename={#MyAppName} {#MyAppVersion} Setup     
OutputDir=Publish            
PrivilegesRequiredOverridesAllowed=dialog  
ShowLanguageDialog=auto
VersionInfoVersion={#MyAppVersion}    

[Languages]                 
Name: "en"; MessagesFile: "compiler:Default.isl"
Name: "de"; MessagesFile: "compiler:Languages\German.isl"

[Tasks]     
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "omsipath\bin\publish\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs

[Icons]                  
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent unchecked
